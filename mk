#! /bin/sh
set -e

say () { printf "mk-$(basename $PWD): $@\n" >&2 ; } 

build () {
    say "Building"
    install
}

edit_rc_file () {
    path="$1"
    to_source="$2"
    if grep "$(basename "$to_source")" "$path" >> /dev/null ; then
        say "File $(basename "$path") already contains $(basename "$to_source")"
    else
        say "Editing $path\n    (sourcing $to_source)."
        printf ". $to_source # Added by pc\n" >> "$path"
    fi
}

make_link () {
    target="$PWD/$2"
    if [ -f "$1" ] || [ -L "$1" ] ; then
        say "There is already a $1."
        return
    fi
    if [ -f "$target" ] ; then
        (
            cd "$(dirname "$1")"
            say "Making $1 -> $target."
            ln -s "$target" "$(basename "$1")"
        )
    else
        say "You need to call 'mk' from the pc/ directory."
    fi
}

install () {
    to_source=$(find $PWD/include/ -type f)
    for f in $to_source ; do
        if ! [ -f "$f" ] ; then
            say "'mk install' should be run from the pc repository."
            return 4
        fi
        edit_rc_file "$HOME/.profile" "$f"
        edit_rc_file "$HOME/.bashrc" "$f"
    done
    make_link $HOME/.screenrc rc/dot_screenrc
    make_link $HOME/.vimrc rc/dot_vimrc.vim
    make_link $HOME/.tmux.conf rc/tmux.conf
}

{ if [ "$1" = "" ] ; then build ; else "$@" ; fi ; }
