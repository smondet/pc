# Manage SSH and GPG keys, and agents.

sgk_say () { printf "[sgk] $@\n" >&2 ; } 

sgk_absolute_path="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/$(basename "${BASH_SOURCE[0]}")"

if grep sgk_absolute_path "$sgk_absolute_path" > /dev/null ; then 
    :
else
    sgk_say "WARNING: failed to fill '\$sgk_absolute_path'\n\
    (value: '$sgk_absolute_path')\n\
    It you are not using 'bash', some functionality will be missing\n\
    (but you can edit the source and just write down it's real path, line 5).\n\
    If you are indeed using 'bash' and see this message you found an actual bug!"
fi


sgk_usage () {
    cat >&2 <<EOF
usage: sgk CMD args...

Commands:
$sgk_commands_help

Source: ${sgk_absolute_path}

This script is meant to be sourced (with '.'), the 'go' command will try to
edit your '~/.profile', '~/.bashrc', '~/.ssh/config' BTW.
EOF
}
sgk_commands_help=""

sgk_commands_help="$sgk_commands_help
* reload: reload \${sgk_absolute_path}
  (= ${sgk_absolute_path})."
sgk_reload () {
    sgk_say "Reloading ${sgk_absolute_path}"
    if [ -f "${sgk_absolute_path}" ] ; then
        . "${sgk_absolute_path}"
    else
        sgk_say "\${sgk_absolute_path} is '${sgk_absolute_path}'\nCannot find it!"
        return 2
    fi
}

sgk_edit_rc_file () {
    path="$1"
    if grep sgk_ssh_gpg_keys.sh "$path" > /dev/null ; then
        :
    else
        (
            cd "$(dirname "$path")"
            if [ -f "${sgk_absolute_path}" ] ; then
                sgk_say "Editing '$path'"
                printf ". ${sgk_absolute_path}\n" >> "$path"
            fi
        )
    fi
}
sgk_edit_ssh_config () {
    path=$HOME/.ssh/config
    if grep 'gpg-connect-agent UPDATESTARTUPTTY' "$path" > /dev/null ; then
        :
    else
        cat >> "$path" <<EOF
Match host * exec "gpg-connect-agent UPDATESTARTUPTTY /bye"
EOF
    fi
}
sgk_write_configuration () {
    case "$1" in
        "terminal" )
            sgk_say "Finding pinentry-tty:"
            if command -v pinentry-tty ; then
                pinentry=$(command -v pinentry-tty)
            else
                sgk_say "FAILURE!!" ; return 4
            fi ;;
        "graphical" | * ) pinentry=/etc/alternatives/pinentry ;;
    esac
    cat > $HOME/.gnupg/gpg-agent.conf <<EOF
# Writen by sgk ($1)
enable-ssh-support
pinentry-program ${pinentry}
allow-loopback-pinentry
EOF
    cat > $HOME/.gnupg/gpg.conf <<EOF
# Writen by sgk
# Disable inclusion of the version string in ASCII armored output
no-emit-version
# Disable comment string in clear text signatures and ASCII armored messages
no-comments
# Avoid SHA1:
personal-digest-preferences SHA256
cert-digest-algo SHA256
EOF
    sgk_edit_rc_file ~/.profile
    sgk_edit_rc_file ~/.bashrc
    sgk_edit_ssh_config
}

sgk_commands_help="$sgk_commands_help
* go {local,graphical} (aliases: l,L,terminal,t,T,g,G):
  update configuration as much as possible."
sgk_go () {
    case "$1" in
        local | l | L | terminal | t | T )
            sgk_say "Local/Terminal setup:"
            sgk_write_configuration terminal
            ;;
        graphical | g | G )
            sgk_say "Graphical setup:"
            sgk_write_configuration graphical
            ;;
        help | -h )
            sgk_usage
            return ;;
        * )
            sgk_say "Wrong argument : '$1'"
            sgk_usage
            return 3 ;;
    esac
    sgk_say "Reloading gpg-agent..."
    gpg-connect-agent reloadagent /bye
    export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
    sgk_say "Telling gpg-agent about current TTY..."
    export GPG_TTY=$(tty)
    echo UPDATESTARTUPTTY | gpg-connect-agent
}

sgk_check_file () {
    path="$1"
    pattern="$2"
    if grep "$pattern" "$path" > /dev/null ; then
        printf "* File '$path' has:\n"
        grep "$pattern" "$path" | sed 's/^/    |/'
    else
        printf "* File '$path' does NOT mention '$pattern'.\n"
    fi
}

sgk_commands_help="$sgk_commands_help
* wtf: display the current state."
sgk_wtf () {
    printf "SGK WTF:\n"
    printf "* File gpg-agent.conf:\n"
    sed 's/^/    |/' ~/.gnupg/gpg-agent.conf
    printf "* File gpg.conf:\n"
    sed 's/^/    |/' ~/.gnupg/gpg.conf
    ssh_expect=$(gpgconf --list-dirs agent-ssh-socket)
    if [ "$SSH_AUTH_SOCK" = "$ssh_expect" ] ; then
        printf "* SSH_AUTH_SOCK = \"$SSH_AUTH_SOCK\" as expected\n"
    else
        printf "* SSH_AUTH_SOCK = \"$SSH_AUTH_SOCK\"\n"
        printf "  should be \"$ssh_expect\"\n"
    fi
    here=$(tty)
    if [ "$GPG_TTY" = "$here" ] ; then
        printf "* \$GPG_TTY is here ($here).\n"
    else
        printf "* \$GPG_TTY is NOT HERE (there: $GPG_TTY, not here: $here).\n"
    fi
    if gpg-connect-agent /bye ; then
        printf "* GPG-Agent is running.\n"
    else
        printf "* GPG-Agent is NOT RUNNING.\n"
    fi
    sgk_check_file  ~/.profile sgk_ssh_gpg_keys.sh
    sgk_check_file  ~/.bashrc sgk_ssh_gpg_keys.sh
    sgk_check_file  ~/.ssh/config UPDATESTARTUPTTY
}

sgk_commands_help="$sgk_commands_help
* runtest: run a quick self-test."
sgk_runtest () {
    sgk_say "Running a quick 'gpg -c':"
    echo "Today is $(date -R)" | gpg -c --armor
}


sgk () {
    if [ "$1" = "" ] || [ "$1" = "--help" ] ; then
        sgk_usage
        return 2
    else
        cmd=sgk_$1
        shift
        $cmd "$@"
    fi
}

# Configurations for every load:
if [ -f ~/.gnupg/gpg-agent.conf ] ; then
    export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
fi

