umask 0077

# Make sure nobody has set a prompt-command
# MacOSX sets and annoying one in /etc/bashrc
# Causing `bash: update_terminal_cwd: command not found`
export PROMPT_COMMAND=""

alias s='cd ..'
alias sps='ps -Af | grep $USER | grep -v "grep"'
alias ls1='\ls -1'
alias ls='ls -hF --color=auto'
alias ll='ls -l'
alias lt='ls -lt'
alias la='ls -a'
alias lla='ls -la'
alias r='cd / ; cd -'
alias gtr='git log --graph --decorate --color --oneline --all'

mkcd () {
    mkdir -p "$1" && cd "$1"
}

ffp () {
    md=${1:-1}
    find "$PWD" -maxdepth "$md" -type f | sort
}

alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

T_help () { ### Show help message
    cat "${BASH_SOURCE[0]}" | grep -E '^T_' | sed 's/T_\([a-z_0-9]*\)[^#]*###\(.*\)$/- \1: \2/'
}
T_wtf () { ### Show wtf's up
    echo "Sessions:"
    tmux list-sessions | sed 's/^/   | /'
    echo "Buffer ($(tmux show-buffer | wc -l) lines):"
    tmux show-buffer | head -n 3 | sed 's/^/   | /'
    echo "Environment:"
    tmux show-env | sed 's/^/   | /'
}
T__ () { ### (noarg) DWIM attach or update environment
    # Weird default but why not:
    if [ "$TMUX_PANE" = "" ] ; then
        tmux -2u attach -d
    else
        echo "Updating environment" >&2
        eval $(tmux show-env -s)
    fi
}
T () {
    if [ "$1" = "" ] ; then
        T__
    else 
        cmd="$1"
        shift
        "T_$cmd" "$@"
    fi
}

G () {
    case "$1" in
        tr ) gtr ;;
        gds )
            git log --graph --decorate --branches  --oneline \
                --simplify-by-decoration --decorate-refs-exclude=refs/tags --all -n 30
            ;;
        gl )
            interesting_branches=$(git branch | sed 's/\*//')
            git log --graph --decorate  --oneline \
                $interesting_branches -n 30
            ;;

        * )
            echo "G: wrong command '$1'" ; return 2 ;;
    esac
}



#------------------------------------------------------------------------------#
# General Variables :
#------------------------------------------------------------------------------#
export EDITOR="vim"
if [ -S "$NVIM" ] ; then
    export PAGER=cat
    export OPAMYES=1
fi
alias ee='$EDITOR'

# start vim to edit a command
vc(){
    echo vc
    local tmp=`mktemp`
    vim $tmp
    . $tmp
    local res=$?
    rm $tmp
    return $res
}

calc () {
  echo "scale=5 ; $*" | bc
}

sshl () {
    this_file="$BASH_SOURCE"
    host="$1"
    if [ "$host" == "" ] ; then
        echo "No \$1 provided :( (this_file: $this_file)" >&2
        return 2
    fi
    if [ "$TMUX_PANE" != "" ]; then
        tmux rename-window "$(basename "$host")";
    fi;
    remote="/tmp/$USER-bashrc.sh"
    scp "$this_file" "$host:$remote"
    shift
    ssh "$host" -t "bash --rcfile $remote" "$@"
}
