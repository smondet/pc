" VIMRC file.

" Ensure no old vi compatibility
set nocompatible

" https://medium.com/usevim/vim-101-virtual-editing-661c99c05847
set virtualedit+=block

" https://superuser.com/questions/242156/make-vim-normal-mode-cursor-sit-between-characters-instead-of-on-them
set selection=exclusive
set virtualedit+=onemore
nnoremap vd <Nop>
noremap p P
noremap a i
" make i<Esc> not move the cursor
inoremap <Esc> <Right><Esc>


" http://stackoverflow.com/questions/1272007/refresh-all-files-in-buffer-from-disk-in-vim
set autoread

" where to put swap files:
let g:swap_dir = $HOME . "/tmp/vimswaps/"
if isdirectory(g:swap_dir) == 0
  call mkdir(g:swap_dir, "p", 0700)
endif
let cmd="set dir=" . g:swap_dir
exec cmd

" Let's try to add a new `ESC`
" C.f. http://vim.wikia.com/wiki/Avoid_the_escape_key
nnoremap <Tab> <Esc>
vnoremap <Tab> <Esc>gV
onoremap <Tab> <Esc>
inoremap <Tab> <Esc>`^

" Quickly move in insert mode
inoremap <M-h> <Left>
inoremap <M-k> <Up>
inoremap <M-j> <Down>
inoremap <M-l> <Right>
inoremap <c-a> <Home>
inoremap <c-e> <End>


" Auto-complete with a better binding
inoremap <C-o> <C-x><C-o>

" Make backspace work everywhere
set backspace=indent,eol,start

"" Turn on fancy stuff
filetype plugin on
filetype indent on
syntax enable
filetype on

" Visual bell
set visualbell

" Be able to have unsaved buffers not visible
set hidden

" <leader> and <localleader>
let mapleader = " "
let g:mapleader = " "
let maplocalleader = "-"
let g:maplocalleader = "-"
" mapping <C-,> is impossible


" Set utf8 as standard encoding and en_US as the standard language
if !has('nvim')
    set encoding=utf8
endif

" Use Unix as the standard file type
set ffs=unix,dos,mac

" Improve timeouts
:set timeout ttimeout timeoutlen=3000 ttimeoutlen=100

" set the completion
set wildmenu
set wildmode=longest:full,list
" This is tab is in the command mode:
set wildchar=<TAB>

" Indentation with spaces
set et
set si    " smartindent
set ai    " autoindent
set smarttab  " use shiftwidth spaces when inserting tab
" Set tab width
set tabstop=8
set shiftwidth=4

" Stop autoformating from plugins:
" We used to accept is 'j' which removes comment-leader while joining
" lines, but it tends to remove more stuff.
au FileType * set formatoptions=

" Cooler joining operators:
:nmap <leader>jn mkJ`k
:nmap <leader>jp mkkJ`k

" Set History length
set history=800

" Status line for single buffer (always)
let g:messages = ''
set stl=[%n]%r%m%h%w%y%<\ %f\ %{g:messages}%=\ (l:%4l/%L),(c:%3c)\ (%2p%%)
set laststatus=2

" Brackets Parentheses Etc.
" Show bracket match
set showmatch
" 'Show bracket matching' time in tenths of a second
set matchtime=2
nmap <leader>ip i()i
nmap <leader>is i""i
nmap <leader>ap a()i
nmap <leader>as a""i

" Search
" Toggle incremental search on
set incsearch
" Switch on search pattern highlighting.
set hlsearch
" Search ignoring case until some uppercase is used:
set ignorecase
set smartcase
" search current selection
vmap <leader>/ y/"
" ignore case search:
nmap <leader>/ /\c

" Folding
highlight Folded cterm=bold term=bold ctermbg=0
let lvl = 0
while lvl <= 9 
    let cmd = ':nmap <leader>v' . lvl . ' :set foldlevel=' . lvl . '<cr>'
    exec cmd
    let lvl += 1
endwhile

" Basic mappings
" Write
nmap <leader>w :w<cr>

" Buffer switching
" Display all buffer and choose
nmap <leader>bl :ls<cr>:b 
" See http://vim.wikia.com/wiki/Cycle_through_buffers_including_hidden_buffers
function! SwitchToNextBuffer(incr)
  let help_buffer = (&filetype == 'help')
  let current = bufnr("%")
  let last = bufnr("$")
  let new = current + a:incr
  while 1
    if new != 0 && bufexists(new) && ((getbufvar(new, "&filetype") == 'help') == help_buffer)
      execute ":buffer ".new
      break
    else
      let new = new + a:incr
      if new < 1
        let new = last
      elseif new > last
        let new = 1
      endif
      if new == current
        break
      endif
    endif
  endwhile
endfunction
nnoremap <silent> <leader>bn :call SwitchToNextBuffer(1)<CR>
nnoremap <silent> <leader>bp :call SwitchToNextBuffer(-1)<CR>


" Save all and make
set makeprg=sh\ mk " The default `make` is the local `mk`
nmap <leader>mk :wa \| make<CR>

" Digraphs
" U+2026 horizontal ellipsis:
digraph .. 8230
" U+160 no-break space
digraph nb 160

" Showing whitespace
set listchars=tab:>-,eol:$,trail:.,extends:>,precedes:<,nbsp:~
nmap <leader>vw :set list!<CR>

let $current_session_dir = $HOME . "/tmp/vim_temp_memory" . $PWD
let $current_session = $current_session_dir . "/seb-vim-default-view-"
let g:current_session = 0
function! Save_session()
    let g:current_session = g:current_session + 1
    let file = $current_session . g:current_session
    exec "mksession! " . file
    echo "Saved to " . file
endfunction
function! Load_session()
    let file = $current_session . g:current_session
    silent only
    silent tabonly
    exec "silent source " . file
    echo "Loaded " . file
endfunction
function! Pop_session()
    let g:current_session = g:current_session - 1
    call Load_session()
endfunction
silent !mkdir -p $current_session_dir
nmap <leader>vs :call Save_session()<cr>
nmap <leader>vl :call Load_session()<cr>
nmap <leader>vp :call Pop_session()<cr>

" Execute commands:
vmap <leader>x "ky:exec "@k"
nmap <leader>x "kyy:exec "@k"

" Paste mode
nmap <leader>pp :setlocal paste!<cr>

" Spell Check
nmap <leader>sc :setlocal spell!<cr>

" Tabs
nmap <leader>tq :tabclose<cr>

set number

" Removed this because it makes (Neo)vim very slow:
"set relativenumber


" hi CursorLineNr   cterm=NONE ctermbg=darkred guibg=darkred
" hi LineNr   cterm=NONE ctermbg=darkblue guibg=darkblue

" Mappings to simplify the access to the '+' register
" i.e. the clipboard
" and also the unnamed one
nnoremap ' "+
vnoremap ' "+
inoremap <c-r>' <c-r>+
inoremap <c-r><c-r> <c-r>"

nmap <leader>rra p 
nmap <leader>rri P 
nmap <leader>r'a "+p 
nmap <leader>r'i "+P 

" Mapping to avoid typing <C-W>
nmap , <C-W>
vmap , <C-W>
" Hope the binding of `<BS>` is temporary:
" https://github.com/neovim/neovim/issues/2048
nnoremap <silent> <BS> <C-W>h
nnoremap <silent> <C-h> <C-W>h:checktime<cr>
nnoremap <silent> <C-j> <C-W>j:checktime<cr>
nnoremap <silent> <C-k> <C-W>k:checktime<cr>
nnoremap <silent> <C-l> <C-W>l:checktime<cr>

" There used to be an `nmap ; :` but it was unwanted, let's try this:
nnoremap ,; q:


" Call xdg-open text object:
" http://stackoverflow.com/questions/5825490/vim-command-with-count-and-motion-possible
if has("mac")
    let g:open_url = 'open'
else
    let g:open_url = 'xdg-open'
endif
function! Open_url(type)
  let sel_save = &selection
  let &selection = "inclusive"
  let reg_save = @@
  if a:0  " Invoked from Visual mode, use '< and '> marks.
      silent exe "normal! `<" . a:type . "`>y"
  elseif a:type == 'line'
      silent exe "normal! '[V']y"
  elseif a:type == 'block'
      silent exe "normal! `[\<C-V>`]y"
  else
      silent exe "normal! `[v`]y"
  endif
  call system(g:open_url . ' "' . @@ . '" &')
  let &selection = sel_save
  let @@ = reg_save
endfunction
nmap <leader>o :set opfunc=Open_url<CR>g@
vmap <leader>o "ky:call system(g:open_url . ' <c-r>k &')<cr>
vmap <leader>gg "ky:call system(g:open_url . ' "http://google.com/search?q=<c-r>k" &')<cr>

" Open a scratch file
autocmd SwapExists ~/scratch.md let v:swapchoice = "e"
autocmd BufNewFile,BufReadPost ~/scratch.md setlocal autoread
autocmd CursorHold ~/scratch.md checktime 
nmap <leader>OS :e! ~/scratch.md<cr>

" Capitalize words
nmap <leader>C mkviwo<esc>~`k

" Delete trailing whitespace
:nnoremap <silent> <leader>dtw :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar>:nohl<CR>

" ------------------------------------------------------------------------------
" Change the binding of `>` amd `<`:
fun! Add_or_remove_indent(number, add_or_remove, visual)
  let stupidlist = [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
  if a:visual == 1
    let prefix = "'<,'>"
  else 
    let prefix = ""
  endif
  if a:add_or_remove == 'add'
    let cmd = prefix . 's/^/' . join(stupidlist[1: a:number], '') . '/e'
  else
    let cmd = prefix . 's/^' . join(stupidlist[1: a:number], '') . '//e'
  endif
  " echo cmd . ' -> ' . a:number . ' -> ' . string(stupidlist[1: a:number])
  exec cmd
  noh
  if a:visual == 1
    normal gv
  endif
endfun
nmap > :<C-U>call Add_or_remove_indent(v:count1,'add',0)<cr>
vmap > :<C-U>call Add_or_remove_indent(v:count1,'add',1)<cr>
nmap < :<C-U>call Add_or_remove_indent(v:count1,'remove',0)<cr>
vmap < :<C-U>call Add_or_remove_indent(v:count1,'remove',1)<cr>

" ------------------------------------------------------------------------------
" Help about complex bindings:
" Help about all the <leader>t bidings.
nmap <leader>t? :nmap <lt>Leader>t<cr>
nmap <leader>v? :nmap <lt>Leader>v<cr>
nmap <leader>p? :nmap <lt>Leader>p<cr>
nmap <leader>s? :nmap <lt>Leader>s<cr>
nmap <leader>m? :nmap <lt>Leader>m<cr>
nmap <leader>i? :nmap <lt>Leader>m<cr>
nmap <leader>r? :nmap <lt>Leader>r<cr>
nmap <leader>g? :nmap <lt>Leader>g<cr>

" ------------------------------------------------------------------------------
" Highlight long lines
" just the 81st column of wide lines...
highlight ColorColumn ctermbg=magenta
call matchadd('ColorColumn', '\%81v', 100)

" ------------------------------------------------------------------------------
" Align command
" found [online](http://vim.wikia.com/wiki/Regex-based_text_alignment)
command! -nargs=? -range Align <line1>,<line2>call AlignSection('<args>')
function! AlignSection(regex) range
  let extra = 1
  let sep = empty(a:regex) ? '=' : a:regex
  let maxpos = 0
  let section = getline(a:firstline, a:lastline)
  for line in section
    let pos = match(line, ' *'.sep)
    if maxpos < pos
      let maxpos = pos
    endif
  endfor
  call map(section, 'AlignLine(v:val, sep, maxpos, extra)')
  call setline(a:firstline, section)
endfunction
function! AlignLine(line, sep, maxpos, extra)
  let m = matchlist(a:line, '\(.\{-}\) \{-}\('.a:sep.'.*\)')
  if empty(m)
    return a:line
  endif
  let spaces = repeat(' ', a:maxpos - strlen(m[1]) + a:extra)
  return m[1] .   spaces . m[2]
endfunction

" ------------------------------------------------------------------------------
" GUI Related
"
" Remove the menu bar and tools bar
if has("gui_running")
    " remove toolbar:
    set guioptions-=T
    " show gui-based tabs:
    set guioptions+=e
    " do not popup dialogs:
    set guioptions+=c
    set guitablabel=%M\ %t
    function! ToggleMenuAndTools()
        if &guioptions =~# 'T'
            set guioptions-=T
            set guioptions-=m
        else
            set guioptions+=T
            set guioptions+=m
        endif 
    endfunction
    nmap <silent> <F4> :call ToggleMenuAndTools()<CR>
"    let g:font_name="DejaVu\\ Sans\\ Mono"
"    let g:font_separator = "\\ "
"    let g:font_size=7
"    let cmd="set gfn=" . g:font_name . g:font_separator . g:font_size
"    execute cmd
"    "   Zoom In et Out
"    function! ZoomIn()
"        let g:font_size=g:font_size + 1
"        let cmd="set gfn=" . g:font_name . g:font_separator . g:font_size
"        execute cmd
"    endfunction
"    function! ZoomOut()
"        let g:font_size=g:font_size - 1
"        let cmd="set gfn=" . g:font_name . g:font_separator . g:font_size
"        execute cmd
"    endfunction
"    colorscheme desert
"    nmap <leader>zi :call ZoomIn()
"    nmap <leader>zo :call ZoomOut()
    set guifont=Monospace\ 16
endif

